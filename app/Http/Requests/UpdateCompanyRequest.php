<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => 'nullable|email|unique:companies,email,'.$this->company->id,
            'name' => 'required|min:3|max:255',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,webp,gif,jfif',
            'website' => 'nullable|url',
        ];
    }
}
