<?php

namespace App\Repositories;

use App\Models\Employee;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class EmpolyeeRepository.
 */
class EmpolyeeRepository extends Repository
{
    protected $model;
    public function __construct(Employee $model)
    {
        $this->model = $model;
    }
    public function create(array $attributes)
    {
        $employee = parent::create(array_merge($attributes));
        return $employee;
    }
    public function update($id, array $attributes)
    {
        $employee =  $this->model->find($id);
        $employee = $employee->update(array_merge($attributes));
        return $employee;
    }
    public function delete($id)
    {
        $employee =  $this->model->find($id);
        $employee = $employee->delete();
        return true;
    }
}
