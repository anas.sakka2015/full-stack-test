@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card mb-4">
            <form action="{{ route('admin.employees.update', $employee->id) }}" method="post" class="p-4"
                enctype="multipart/form-data">
                @csrf
                @method('put')
                <h5 class="card-header">Update Employee Information</h5>
                <div class="card-body demo-vertical-spacing demo-only-element">
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAcxJREFUSEvlle0xBEEQhp+LgAwQASJABmRABIgAESACIZABIkAEhEAE1KNmrub2ZmZ7+eGHrrrarduefvt9pz9m/JHN/giXnwLvApsp6WfgcSqBqcD7wA2wOgB6B46Au2gCU4CvgOORwKeAfqMWBVba+xRNWQ3+AMhUFU6AnfR9L33rgkeBvUfv9AXYakR8A9YAfbfHKEeBP1OgnpTnwFnyG4076gCUMvdkjPp95xYBXgdeE5ODTuUeporXdQNQ+qZFgD1sEa0A16mQagFz1X9U2m3JPwpctlJN7lLmXnLzBKLADgzbJ08r363e3E650ntVv8A6Cuwh79rJlMGH8tnf3nP3bvOhKcD5jMPCoZFZytyEQhPrN8BjsyH0PcpYdkrsszW5ZO7Pe/b5q5FpUbmNlHaKKb3byuKrWo9xbQXaoy02KmGvZ+uuyhawQZ6KIPamxTNWsVa+xVeuTxfGUrIt4LyNZChz+3aKeeY2HahuqxpwuWXCi72Slcwv0/8XgHHnVgOWnUs9PIU6UmTlHC6O1S5w3r1LWU7ROvk2d/SQcVlUvRUYzaG864UiGwJPWuYB9Ga8IbADo5zBzQEQANWlGS86MoM4cbf/B/wF5GpcH3yxaxIAAAAASUVORK5CYII=" />
                        {{-- <span >Full name</span> --}}
                        <input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                            placeholder="first_name" aria-label="first_name" value="{{ $employee->first_name }}"
                            name='first_name' required />
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAcxJREFUSEvlle0xBEEQhp+LgAwQASJABmRABIgAESACIZABIkAEhEAE1KNmrub2ZmZ7+eGHrrrarduefvt9pz9m/JHN/giXnwLvApsp6WfgcSqBqcD7wA2wOgB6B46Au2gCU4CvgOORwKeAfqMWBVba+xRNWQ3+AMhUFU6AnfR9L33rgkeBvUfv9AXYakR8A9YAfbfHKEeBP1OgnpTnwFnyG4076gCUMvdkjPp95xYBXgdeE5ODTuUeporXdQNQ+qZFgD1sEa0A16mQagFz1X9U2m3JPwpctlJN7lLmXnLzBKLADgzbJ08r363e3E650ntVv8A6Cuwh79rJlMGH8tnf3nP3bvOhKcD5jMPCoZFZytyEQhPrN8BjsyH0PcpYdkrsszW5ZO7Pe/b5q5FpUbmNlHaKKb3byuKrWo9xbQXaoy02KmGvZ+uuyhawQZ6KIPamxTNWsVa+xVeuTxfGUrIt4LyNZChz+3aKeeY2HahuqxpwuWXCi72Slcwv0/8XgHHnVgOWnUs9PIU6UmTlHC6O1S5w3r1LWU7ROvk2d/SQcVlUvRUYzaG864UiGwJPWuYB9Ga8IbADo5zBzQEQANWlGS86MoM4cbf/B/wF5GpcH3yxaxIAAAAASUVORK5CYII=" />
                        {{-- <span >Full name</span> --}}
                        <input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                            placeholder="last_name" aria-label="last_name" value="{{ $employee->last_name }}"
                            name='last_name' required />
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="input-group">

                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAMlJREFUSEvtlkERwjAQRV8d4KCgAJwUBwwOcAAOigRwAApAAhLACbOHXGgmGzYp5bA99+f1b392fsNETzMRlxzwDOiAueEjr8AjptPAAr0BKwM0SLbA6VOvgXdAXwAV6RNYfAs+APtCsMgHBjXHDrZO3UetLhAPl4crdwJ+nfw6DbLytwvkDFyAI9AmIl491eFAqTabX4IFGBynymB1x6MtkBpl7xWrxlrnknp7B5a51iLvmeqtnCPwtbHQy/83FfoCo2mpNurRwG/UcSUfMoEwVwAAAABJRU5ErkJggg==" />
                        <input type="email" required class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                            placeholder="email" aria-label="email" name="email" value="{{ $employee->email }}" />
                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAcxJREFUSEvlle0xBEEQhp+LgAwQASJABmRABIgAESACIZABIkAEhEAE1KNmrub2ZmZ7+eGHrrrarduefvt9pz9m/JHN/giXnwLvApsp6WfgcSqBqcD7wA2wOgB6B46Au2gCU4CvgOORwKeAfqMWBVba+xRNWQ3+AMhUFU6AnfR9L33rgkeBvUfv9AXYakR8A9YAfbfHKEeBP1OgnpTnwFnyG4076gCUMvdkjPp95xYBXgdeE5ODTuUeporXdQNQ+qZFgD1sEa0A16mQagFz1X9U2m3JPwpctlJN7lLmXnLzBKLADgzbJ08r363e3E650ntVv8A6Cuwh79rJlMGH8tnf3nP3bvOhKcD5jMPCoZFZytyEQhPrN8BjsyH0PcpYdkrsszW5ZO7Pe/b5q5FpUbmNlHaKKb3byuKrWo9xbQXaoy02KmGvZ+uuyhawQZ6KIPamxTNWsVa+xVeuTxfGUrIt4LyNZChz+3aKeeY2HahuqxpwuWXCi72Slcwv0/8XgHHnVgOWnUs9PIU6UmTlHC6O1S5w3r1LWU7ROvk2d/SQcVlUvRUYzaG864UiGwJPWuYB9Ga8IbADo5zBzQEQANWlGS86MoM4cbf/B/wF5GpcH3yxaxIAAAAASUVORK5CYII=" />
                        {{-- <span >Full name</span> --}}
                        <input type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}"
                            placeholder="phone" aria-label="phone" value="{{ $employee->phone }}" name='phone'
                            required />
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">

                        <select name="company_id"
                            class="form-select form-select-lg {{ $errors->has('company_id') ? ' is-invalid' : '' }}">
                            <option value="">
                                select company
                            </option>
                            @foreach ($companies as $company)
                                <option value="{{ $company->id }}"
                                    {{ $employee->company_id == $company->id ? 'selected' : '' }}>
                                    {{ $company->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('company_id'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('company_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    {{-- <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAPVJREFUSEvtl40RwUAQhb90oAM6QAXogA6UoAQ6oQI6QAdKoAJKMC9zMXETdzkuMsztTCaTZPPe7mb/ktGSZC3x4iKeAv0Iht2ADaDzQ6qIB8AW6EUgLSBOwNBHLKUYntp2i1jYudged4Drm54egbH1rq735t4EOLwiLiuG8idiRSyF+j+TS41nZLrVV76xmoOOuanDM7ADFk3Xsa/uG2sgidiOQAr1U0Q+mU6/n1wXYO1zI+C5sNRUKjeQcqhnpvMEYNdXda0+S2BVHypMs2rLbGrZc9axzNZ00dzshvng1fYSFwha6LVjxxJncsUi8eK09u90Bw1yTB+z/MtBAAAAAElFTkSuQmCC" />
                        <input type="file" class="form-control" placeholder="Passport" aria-label="Passport"
                            name="passport" value="{{ $employee->passport }}" />
                    </div> --}}
                    <button class="btn btn-primary btn-style" type="submit">Update</button>
                    {{-- <div class="form-password-toggle">
                    <label class="form-label " for="basic-default-password12">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" id="basic-default-password12"
                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                            aria-describedby="basic-default-password2" />
                        <span id="basic-default-password2" class="input-group-text cursor-pointer"><i
                                class="bx bx-hide"></i></span>
                    </div>
                </div> --}}
                </div>
            </form>
        </div>
    </div>
@endsection
