@extends('layouts.app')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Tables /</span> Employee</h4>

        <!-- Basic Bootstrap Table -->

        <div class="card h-100">
            <div class="row">
                <div class="col-lg-10 col-12">
                    <h5 class="card-header">Employee Table </h5>
                </div>
                <div class="col-lg-2 col-12 p-3">
                    <a href="{{ route('admin.employees.create') }}" class="btn btn-primary btn-block w-100 ">
                        Add Employee
                    </a>
                </div>
            </div>
            <div class="table-responsive text-nowrap h-100">

                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Company name</th>
                            <th>Phone</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-1 ">
                        @include('dashboard.employee.table')
                    </tbody>
                </table>
                <div class="row align-items-center text-center mt-5">
                    <div class="col-12 col-lg-4 "></div>
                    <div class="col-12 col-lg-4 text-center ">
                        <div class="pageination text-center ">
                            {!! $employees->links() !!}
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 "></div>
                </div>
            </div>
        </div>
        <hr class="my-5" />
    </div>
@endsection
