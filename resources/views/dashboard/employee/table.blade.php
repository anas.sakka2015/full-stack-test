@foreach ($employees as $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->first_name }}</td>
        <td>{{ $item->last_name }}</td>
        <td>{{ $item->email }}</td>
        <td>{{ $item->company_id }}</td>
        <td>{{ $item->phone }}</td>

        <td>
            <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                    <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('admin.employees.edit', $item->id) }}"><i
                            class="bx bx-edit-alt me-1"></i>
                        Edit</a>
                    <a class="dropdown-item deleteItem" data-url='{{ route('admin.employees.destroy', $item->id) }}'
                        href="#"><i class="bx bx-trash me-1"></i>
                        Delete</a>
                </div>

            </div>
        </td>
    </tr>
@endforeach
