@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card mb-4">
            <form action="{{ route('admin.companies.update', $company->id) }}" method="post" class="p-4"
                enctype="multipart/form-data">
                @csrf
                @method('put')
                <h5 class="card-header">Update Company Information</h5>
                <div class="card-body demo-vertical-spacing demo-only-element">
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAcxJREFUSEvlle0xBEEQhp+LgAwQASJABmRABIgAESACIZABIkAEhEAE1KNmrub2ZmZ7+eGHrrrarduefvt9pz9m/JHN/giXnwLvApsp6WfgcSqBqcD7wA2wOgB6B46Au2gCU4CvgOORwKeAfqMWBVba+xRNWQ3+AMhUFU6AnfR9L33rgkeBvUfv9AXYakR8A9YAfbfHKEeBP1OgnpTnwFnyG4076gCUMvdkjPp95xYBXgdeE5ODTuUeporXdQNQ+qZFgD1sEa0A16mQagFz1X9U2m3JPwpctlJN7lLmXnLzBKLADgzbJ08r363e3E650ntVv8A6Cuwh79rJlMGH8tnf3nP3bvOhKcD5jMPCoZFZytyEQhPrN8BjsyH0PcpYdkrsszW5ZO7Pe/b5q5FpUbmNlHaKKb3byuKrWo9xbQXaoy02KmGvZ+uuyhawQZ6KIPamxTNWsVa+xVeuTxfGUrIt4LyNZChz+3aKeeY2HahuqxpwuWXCi72Slcwv0/8XgHHnVgOWnUs9PIU6UmTlHC6O1S5w3r1LWU7ROvk2d/SQcVlUvRUYzaG864UiGwJPWuYB9Ga8IbADo5zBzQEQANWlGS86MoM4cbf/B/wF5GpcH3yxaxIAAAAASUVORK5CYII=" />
                        {{-- <span >Full name</span> --}}
                        <input type="text" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                            placeholder="name" aria-label="name" value="{{ $company->name }}" name='name' />
                        @if ($errors->has('name'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">

                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAMlJREFUSEvtlkERwjAQRV8d4KCgAJwUBwwOcAAOigRwAApAAhLACbOHXGgmGzYp5bA99+f1b392fsNETzMRlxzwDOiAueEjr8AjptPAAr0BKwM0SLbA6VOvgXdAXwAV6RNYfAs+APtCsMgHBjXHDrZO3UetLhAPl4crdwJ+nfw6DbLytwvkDFyAI9AmIl491eFAqTabX4IFGBynymB1x6MtkBpl7xWrxlrnknp7B5a51iLvmeqtnCPwtbHQy/83FfoCo2mpNurRwG/UcSUfMoEwVwAAAABJRU5ErkJggg==" />
                        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                            placeholder="email" aria-label="email" name="email" value="{{ $company->email }}" />
                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAWxJREFUSEvtluExBEEQRt9FQAZk4GQgA0SACI4IEAEiIANE4EIgAzIgAupdTauptTdr76Zu/2z/292Zft1fd8/shIFsMhCXEbwx5ZtSnwKXwG7lCN6Ba+Ah/ObgA+ClMrDpbh949WUOvkrZ+t7oapoqhl85S8F+rAVvJlQEG9QTcAZ8rpj6NnAPHGX7TaYT7Hqb4jjq0iOAKfDY0qSd4GfgMIHM+CLvyI4AnIwbwIy13Fcn2KY7Tw6C4ygofclcc5ItMOBb4Pu/zRXd7ohZ66200VFQekuQm3OvtEqsfaXaztNzb7D7lEwHe5n0wsOpwQkNad8A3+VNuRI4MlOyWZam3akyMaN+ukslapZjLbDObBwDCOkDoLT2xO+R2CCvDdafdRQQ0iutAS2OwiVWBRx1N3PNTLsOmWrgQnKtn0Zw8XbqK2dp/eJiSDfen0tisB+BmFMj2qmZLvCRrsPWX5/KrLK78b96Y3IPJvUPSg9eH6QClw8AAAAASUVORK5CYII=" />
                        <input type="text" class="form-control {{ $errors->has('website') ? ' is-invalid' : '' }}"
                            placeholder="website" aria-label="website" value="{{ $company->website }}" name="website">
                        @if ($errors->has('website'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('website') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">
                        <img src="@if (substr($company->logo, 0, 8) != 'https://') {{ asset('storage/' . $company->logo) }} @else {{ asset($company->logo) }} @endif"
                            id="output" class="rounded my-2" alt="" width="150px" height="150px">
                        <label class="form-label w-100 " for="basic-default-password12">logo</label><br>
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAPVJREFUSEvtl40RwUAQhb90oAM6QAXogA6UoAQ6oQI6QAdKoAJKMC9zMXETdzkuMsztTCaTZPPe7mb/ktGSZC3x4iKeAv0Iht2ADaDzQ6qIB8AW6EUgLSBOwNBHLKUYntp2i1jYudged4Drm54egbH1rq735t4EOLwiLiuG8idiRSyF+j+TS41nZLrVV76xmoOOuanDM7ADFk3Xsa/uG2sgidiOQAr1U0Q+mU6/n1wXYO1zI+C5sNRUKjeQcqhnpvMEYNdXda0+S2BVHypMs2rLbGrZc9axzNZ00dzshvng1fYSFwha6LVjxxJncsUi8eK09u90Bw1yTB+z/MtBAAAAAElFTkSuQmCC" />
                        <input type="file"
                            onchange="document.getElementById('output').src = window.URL.createObjectURL(this.files[0])"
                            class="form-control" placeholder="logo" aria-label="logo" name="logo" value="" />
                    </div>
                    {{-- <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAPVJREFUSEvtl40RwUAQhb90oAM6QAXogA6UoAQ6oQI6QAdKoAJKMC9zMXETdzkuMsztTCaTZPPe7mb/ktGSZC3x4iKeAv0Iht2ADaDzQ6qIB8AW6EUgLSBOwNBHLKUYntp2i1jYudged4Drm54egbH1rq735t4EOLwiLiuG8idiRSyF+j+TS41nZLrVV76xmoOOuanDM7ADFk3Xsa/uG2sgidiOQAr1U0Q+mU6/n1wXYO1zI+C5sNRUKjeQcqhnpvMEYNdXda0+S2BVHypMs2rLbGrZc9axzNZ00dzshvng1fYSFwha6LVjxxJncsUi8eK09u90Bw1yTB+z/MtBAAAAAElFTkSuQmCC" />
                        <input type="file" class="form-control" placeholder="Passport" aria-label="Passport"
                            name="passport" value="{{ $company->passport }}" />
                    </div> --}}
                    <button class="btn btn-primary btn-style" type="submit">Update</button>
                    {{-- <div class="form-password-toggle">
                    <label class="form-label " for="basic-default-password12">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" id="basic-default-password12"
                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                            aria-describedby="basic-default-password2" />
                        <span id="basic-default-password2" class="input-group-text cursor-pointer"><i
                                class="bx bx-hide"></i></span>
                    </div>
                </div> --}}
                </div>
            </form>
        </div>
    </div>
@endsection
