@foreach ($companies as $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->email }}</td>
        <td><img src="@if (substr($item->logo, 0, 8) != 'https://') {{ asset('storage/' . $item->logo) }} @else {{ $item->logo }} @endif "
                class="rounded" width="100px" width="100px" alt="">
        </td>
        <td>{{ $item->website }}</td>
        <td>
            <div class="dropdown">
                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                    <i class="bx bx-dots-vertical-rounded"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('admin.companies.edit', $item->id) }}"><i
                            class="bx bx-edit-alt me-1"></i>
                        Edit</a>
                    <a class="dropdown-item deleteItem" data-url='{{ route('admin.companies.destroy', $item->id) }}'
                        href="#"><i class="bx bx-trash me-1"></i>
                        Delete</a>
                </div>

            </div>
        </td>
    </tr>
@endforeach
